package Controller;

import Model.Software;
import Model.SoftwareList;
import Utils.Alerts;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class ManageController {

    @FXML
    private TextField addNameTxtField;

    @FXML
    private TextField addPathTxtField;

    @FXML
    private ListView<Software> editSoftwareListView;

    @FXML
    private TextField editNameTxtField;

    @FXML
    private TextField editPathTxtField;

    @FXML
    private ListView<Software> removeSoftwareListView;

    private SoftwareList softwareList;

    public void initializeData(SoftwareList softwareList){
        this.softwareList = softwareList;
        fillListView();
    }

    @FXML
    void handleAddSoftware(ActionEvent event) {
        if (!addNameTxtField.getText().isEmpty() && !addPathTxtField.getText().isEmpty()){
            Software software = softwareList.newSoftware(addNameTxtField.getText(), addPathTxtField.getText());
            if (softwareList.validateSoftware(software)){
                softwareList.addSoftware(software);
                fillListView();
                removeText();
                Alerts.informrAlert("Software added", null, "The software you selected has been added.");
            } else {
                Alerts.errorAlert("Error!", null, "The software you tried to add is already registered in the system.");
            }
        } else {
            Alerts.errorAlert("Error!", null, "Either the path or the name of the software hasn't been added.");
        }
    }

    @FXML
    void handleEdit(ActionEvent event) {
        if (!editNameTxtField.getText().isEmpty() && !editPathTxtField.getText().isEmpty() && editSoftwareListView.getSelectionModel().getSelectedItem().toString() != null){
            softwareList.editSoftware(editSoftwareListView.getSelectionModel().getSelectedItem(), editNameTxtField.getText(), editPathTxtField.getText());
            fillListView();
            removeText();
            Alerts.informrAlert("Software edited", null, "The software you selected has been edited.");
        } else {
            Alerts.errorAlert("Error!", null, "Either a software hasn't been selected or the new path or name of the software hasn't been added.");
        }
    }

    @FXML
    void handlePathChooserAdd(ActionEvent event) {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose a software");
        File file = fileChooser.showOpenDialog(stage);
        if (file != null){
            addPathTxtField.setText(file.getAbsolutePath());
        }
    }

    @FXML
    void handlePathChooserEdit(ActionEvent event) {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose a software");
        File file = fileChooser.showOpenDialog(stage);
        if (file.exists()){
            editPathTxtField.setText(file.getAbsolutePath());
        }
    }

    @FXML
    void handleRemove(ActionEvent event) {
        if (!removeSoftwareListView.getSelectionModel().getSelectedItems().isEmpty()){
            boolean decision;
            if (removeSoftwareListView.getSelectionModel().getSelectedItems().size() > 1){
                decision = Alerts.confirmAlert("Remove a software", null, "Do you really wish to remove these software?");
            } else {
                decision = Alerts.confirmAlert("Remove a software", null, "Do you really wish to remove this software?");
            }
            if (decision){
                for (Software software : removeSoftwareListView.getSelectionModel().getSelectedItems()){
                    softwareList.removeSoftware(software);
                }
                fillListView();
                removeText();
                Alerts.informrAlert("Software removed", null, "The software you selected has been removed.");
            }
        } else {
            Alerts.errorAlert("Error!", null, "You haven't selected any software to remove. (You can choose more than one with CTRL + Click)");
        }
    }

    void fillListView(){
        editSoftwareListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        removeSoftwareListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        editSoftwareListView.setItems(FXCollections.observableArrayList(softwareList.getSoftwareList()));
        removeSoftwareListView.setItems(FXCollections.observableArrayList(softwareList.getSoftwareList()));
    }

    void removeText(){
        addNameTxtField.clear();
        addPathTxtField.clear();
        editNameTxtField.clear();
        editPathTxtField.clear();
    }

}

