package Controller;

import Model.Software;
import Model.SoftwareList;
import Utils.Alerts;
import Utils.fileManager;
import Utils.onCloseStage;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private MenuBar settingsMenuBar;

    @FXML
    private CheckMenuItem closeLaunchingMenuIItem;

    @FXML
    private ComboBox<String> softwareComboBox;

    @FXML
    private Label noSoftwareLabel;

    private SoftwareList list;


    /**
     * When starting the scene, this method will be the first to execute, and so will load the software list into the combo box.
     * @param list = the list of the software that has already been added
     */
    public void initializeData(SoftwareList list){
        this.list = list;
        for (Software software : this.list.getSoftwareList()){
            softwareComboBox.getItems().add(software.getName());
        }
        if (list.getSoftwareList().isEmpty()){
            noSoftwareLabel.setVisible(true);
        }
    }

    /**
     * This method launches the ManageSoftware_GUI when pressing the menu item
      * @param event = the click of the "Manage software list" menu item
     */
    @FXML
    void handleManageSoftware(ActionEvent event) {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../UI/ManageSoftware_GUI.fxml"));
            Parent manage = loader.load();

            Scene manageGUI = new Scene(manage);

            ManageController controller = loader.getController();
            controller.initializeData(list);

            Stage primaryStage = new Stage();

            primaryStage.setResizable(false);
            primaryStage.setTitle("Software Launcher");
            primaryStage.setScene(manageGUI);

            onCloseStage.onCloseManage(primaryStage, list);

            primaryStage.show();
            ((Stage) noSoftwareLabel.getScene().getWindow()).close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    /**
     * Closes the scene when clicking the button making sure that the backup is saved.
     * @param event = Click of the "Close" menu item
     * @throws IOException
     */
    @FXML
    void handleClose(ActionEvent event) throws IOException {
        fileManager.saveBackup(list);
        ((Stage) anchorPane.getScene().getWindow()).close();
    }

    /**
     * Launches the selected software, making sure an item on the combo box is selected, and closes the scene if the Menu Check Box is selected
     * @param event = Click of the "Launch" button
     * @throws IOException
     */
    @FXML
    void handleLaunch(ActionEvent event) throws IOException {
        if (softwareComboBox.getValue() != null){
            list.getSoftwareByName(softwareComboBox.getValue()).runFile();
            if (closeLaunchingMenuIItem.isSelected()){
                ((Stage) anchorPane.getScene().getWindow()).close();
            }
        } else {
            Alerts.errorAlert("Error!", "No software selected.", "You haven't selected any software to launch. Please select one and try again.");
        }
    }

}
