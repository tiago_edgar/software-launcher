package Controller;

import Model.SoftwareList;
import Utils.fileManager;
import Utils.onCloseStage;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        try{

            SoftwareList list = new SoftwareList();

            if (fileManager.backupFile.exists()){
                list = fileManager.readBackup();
            }



            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../UI/Main_GUI.fxml"));
            Parent main = loader.load();

            Scene mainGUI = new Scene(main);

            Controller controller = loader.getController();
            controller.initializeData(list);

            primaryStage.setResizable(false);
            primaryStage.setTitle("Software Launcher");
            primaryStage.setScene(mainGUI);

            onCloseStage.onCloseStage(primaryStage, list);

            primaryStage.show();
        } catch (Exception e){
            System.out.println(e);
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
