package Model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SoftwareList implements Serializable {

    private List<Software> softwareList = new ArrayList<>();

    public Software newSoftware(String name, String filePath){
        try{
            Software software = new Software(name, new File(filePath));
            return software;
        }catch (Exception e){
            return null;
        }
    }

    public boolean validateSoftware(Software software){
        for (Software added : softwareList){
            if (added.equals(software) || added.getFile().getAbsolutePath().equals(software.getFile().getAbsolutePath()) || added.getName().equals(software.getName())){
                return false;
            }
        }
        return true;
    }

    public void addSoftware(Software software){
        softwareList.add(software);
    }

    public Software getSoftwareByName(String name){
        for (Software software : softwareList){
            if (software.getName().equals(name)){
                return software;
            }
        }
        return null;
    }

    public void editSoftware(Software oldSoftware, String name, String path){
        oldSoftware.setFile(new File(path));
        oldSoftware.setName(name);
    }

    public void removeSoftware(Software software){
        softwareList.remove(software);
    }

    public List<Software> getSoftwareList() {
        return softwareList;
    }
}
