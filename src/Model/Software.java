package Model;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class Software implements Serializable {

    private String name;
    private File file;

    public Software(String name, File file) {
        this.name = name;
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public boolean equals(Software other){
        return name.equals(other.getName()) && file.getAbsolutePath().equals(other.getFile().getAbsolutePath());
    }

    public void runFile() throws IOException {
        Desktop.getDesktop().open(file);
    }

    @Override
    public String toString() {
        return name + "\n" + file.getAbsolutePath();
    }
}
