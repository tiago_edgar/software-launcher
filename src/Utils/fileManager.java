package Utils;

import Model.SoftwareList;

import java.io.*;

public class fileManager {

    public static final File backupFile = new File(System.getenv("Appdata") + "//softwareLauncher//softwareList.backup");

    public static SoftwareList readBackup() throws IOException{
        FileInputStream backup = null;
        try{
            backup = new FileInputStream(backupFile);
            ObjectInputStream in = new ObjectInputStream(backup);
            SoftwareList softwareList = (SoftwareList) in.readObject();

            return softwareList;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (backup != null){
                backup.close();
            }
        }
    }

    public static void saveBackup(SoftwareList softwareList) throws IOException {
        if (softwareList != null){
            FileOutputStream backup = null;
            try {
                backup = new FileOutputStream(backupFile);
                ObjectOutputStream out = new ObjectOutputStream(backup);

                out.writeObject(softwareList);
            } catch (IOException e){

            } finally {
                if (backup != null){
                    backup.close();
                }
            }
        }

    }

}
