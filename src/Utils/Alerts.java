package Utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class Alerts {

    private static Alert alert = new Alert(Alert.AlertType.NONE);

    public static void errorAlert(String title, String header, String content){

        alert.setAlertType(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        alert.showAndWait();
    }

    public static boolean confirmAlert(String title, String header, String content){

        alert.setAlertType(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        if (alert.showAndWait().get() == ButtonType.OK){
            return true;
        } else {
            return false;
        }

    }

    public static void informrAlert(String title, String header, String content){

        alert.setAlertType(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        alert.showAndWait();
    }

}
