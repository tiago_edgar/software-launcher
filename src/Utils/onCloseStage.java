package Utils;

import Controller.Controller;
import Model.SoftwareList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

public class onCloseStage {

    public static void onCloseStage(Stage stage, SoftwareList softwareList){
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try {
                    fileManager.saveBackup(softwareList);
                } catch (IOException e){

                }
            }
        });
    }

    public static void onCloseManage(Stage stage, SoftwareList softwareList){
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try {
                    fileManager.saveBackup(softwareList);
                    try{
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("../UI/Main_GUI.fxml"));
                        Parent manage = loader.load();

                        Scene manageGUI = new Scene(manage);

                        Controller controller = loader.getController();
                        controller.initializeData(softwareList);

                        Stage primaryStage = new Stage();

                        primaryStage.setResizable(false);
                        primaryStage.setTitle("Software Launcher");
                        primaryStage.setScene(manageGUI);

                        onCloseStage.onCloseStage(primaryStage, softwareList);

                        primaryStage.show();
                    } catch (Exception e){
                        System.out.println(e);
                    }
                } catch (IOException e){

                }
            }
        });
    }

}
